#include "BleDevice.h"
#include "defines.h"

BleDevice::BleDevice() : ble(P_TX, P_RX, 115200)
{

}

BleDevice::BleDevice(PinName tx_pin, PinName rx_pin, int baud, RawSerial* pc_p) : ble(tx_pin, rx_pin, baud)
{
    this->pc_p = pc_p;
    reader.attach_us( callback(this, &BleDevice::ProcessBleInput), 2000);
}

BleDevice::~BleDevice()
{

}

void BleDevice::Process()
{
    //if(scanningActive)
        //ProcessBleInput();
}

/**
 * @brief Sends three '$' characters with a 100 ms sleep after the first one. 
 * 
 */
void BleDevice::EnterCommandMode()
{
    //ble.putc('$');
    ble.putc('$');
    wait_ms(100);
    ble.putc('$');
    ble.putc('$');
    wait_ms(10);
    //ProcessBleInput();
    //     wait_ms(10);
    // ProcessBleInput();
    //     wait_ms(10);
    // ProcessBleInput();
    //     wait_ms(10);
    // ProcessBleInput();
}

void BleDevice::ProcessBleInput()
{
    int recvCnt = 0;
    static uint8_t recvBuf[50];
    //pc_p->printf("DEBUG: START BLE READ");
    while(ble.readable())
    {
        recvBuf[recvCnt] = ble.getc();
        recvCnt++;
        if(recvCnt == 50)
            break;
    }
    for(int echoCnt = 0; echoCnt < recvCnt; echoCnt++)
        pc_p->printf("%c", recvBuf[echoCnt]);

    reader.attach_us( callback(this, &BleDevice::ProcessBleInput), 2000);
}

void BleDevice::SendByte(uint8_t byte)
{
    ble.putc(byte);
}

void BleDevice::SendCmdEnd()
{
    ble.putc(10);
    // ProcessBleInput();
}

void BleDevice::SetAdvertisementStrength(uint8_t strength)
{
    SendByte('S');
    SendByte('G');
    SendByte('A');
    SendByte(',');
    SendByte('0');
    SendCmdEnd();
    //wait_ms(100);
    // ProcessBleInput();
}

void BleDevice::StartScanning()
{
    SendByte('F');
    SendCmdEnd();
    scanningActive = true;
    // ProcessBleInput();
}

void BleDevice::StopScanning()
{
    SendByte('X');
    SendCmdEnd();
    // ProcessBleInput();
    scanningActive = false;
}