#ifndef BUFFER_H
#define BUFFER_H

#include "mbed.h"

#define MAX_SIZE 10

template<typename ITEM>
class Buffer
{

private:
    ITEM buffer[MAX_SIZE];
    int count;
    int in;
    int out;
    
public:
    
    Buffer()
    {
        count = 0;
        in = 0;
        out = 0;
    }
    ~Buffer()
    {

    }

    ITEM get()
    {
        ITEM l_retval;
        if(count == 0)
        {
            return NULL;
        }
        else
        {
            l_retval = buffer[out];
            out = (out + 1) % MAX_SIZE;
            --count;
            return l_retval;
        }
    }

    bool add(ITEM p_item)
    {
        if(count >= MAX_SIZE)
        {
            return false;
        }
        else
        {
            buffer[in] = p_item;
            in = (in + 1) % MAX_SIZE;
            ++count;
            return true;
        }
    }

    bool has_free_spot()
    {
        if(count >= MAX_SIZE)
        {
            return false;
        }
        return true;
    }

    ITEM peek()
    {
        if(count == 0)
        {
            return NULL;
        }
        else
        {
            return buffer[out];
        }
    }
};

#endif //BUFFER_H