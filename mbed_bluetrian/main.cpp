#include "mbed.h"
#include "Serial.h"
#include "BleDevice.h"
#include "defines.h"

#define START_SCAN 0x05
#define STOP_SCAN 0x06
#define SET_ADV_STR 0x07

//some changes here
#define PLATFORM FRDMK64F

Serial pc(USBTX, USBRX, 115200);
Serial ble(P_TX, P_RX, 115200);
//BleDevice bleDev(P_TX, P_RX, 115200, &pc);
//Serial ble(P_TX, P_RX, 2400);
// DigitalIn ready(D3);
DigitalOut rst(A2);

// DigitalOut rts(D9);


void readCallBack(int i)
{
    pc.printf("read %d bytes\r\n", i);
}

void writeCallBack(int i)
{
    pc.printf("written %d bytes", i);
}

void DoNothing()
{
    int i = 0;
    i++;

}

void processPcInput();
// void processBleInput();

int main()
{
    while(1)
    {
        while(pc.readable() )
        {
            uint8_t temp = pc.getc();
            if(temp == 13)
                ble.putc(10);
            else
                ble.putc(temp);
        }
        while(ble.readable())
            pc.putc(ble.getc());
    }

//     //reader.attach_us(DoNothing, 10000);
//     wait_ms(2000);
//     pc.printf("Reset of mbed. Configuring BLE\r\n");
//     bleDev.EnterCommandMode();
//     wait_ms(10);
//     bleDev.SendCmdEnd();
//     wait_ms(10);
//     bleDev.SetAdvertisementStrength(0);
//     wait_ms(10);
//    // ble.format(8, SerialBase::Parity::None, 1);
//    // ble.set_flow_control(mbed::SerialBase::Flow::Disabled);

//     pc.printf("All booted up \r\n");
//     while(true)
//     {
//         processPcInput();
//         bleDev.Process();        
//     }
}

void processPcInput()
{
//     while(pc.readable())
//     {
//         switch(pc.getc())
//         {
//         case 0xDD:
//         {
//             pc.printf("Received preCmd -> ");
//             uint8_t AfterCmd = pc.getc();
//             pc.printf("%#4x \r\n", AfterCmd);
//             switch(AfterCmd)
//             {
//                 case 0x02:
//                 {
//                     //rts = 0;
//                     pc.printf("rts low \r\n");
//                 }
//                 break;
//                 case 0x01:
//                 {
//                     //rts = 1;
//                     pc.printf("rts hi \r\n");
//                 }
//                 break;
//                 case 0x04:
//                 {
//                     rst = 0;
//                     pc.printf("rst low \r\n");
//                 }
//                 break;
//                 case 0x03:
//                 {
//                     rst = 1;
//                     pc.printf("rst hi \r\n");
//                 }
//                 break;
//                 case START_SCAN:
//                 {
//                     bleDev.StartScanning();
//                 }
//                 break;
//                 case STOP_SCAN:
//                 {
//                     bleDev.StopScanning();
//                 }
//                 break;
//                 case SET_ADV_STR:
//                 {
//                     bleDev.SetAdvertisementStrength(0);
//                 }
//                 break;

// #define STOP_SCAN 0x06
// #define SET_ADV_STR 0x07
                
//             }
//         }
//         break;
//         case 0xD1:
//         {
//             pc.printf("Received some data to send ->");
//             uint8_t nrOfBytes = pc.getc();
//             uint8_t bytes[nrOfBytes];
//             //pc.printf("%d bytes ->", nrOfBytes);
//             for(int i = 0; i < nrOfBytes; i++)
//             {
//                 bytes[i] = pc.getc();
//                 //pc.printf("%c", bytes[i]);
//             }
//             wait_ms(10);
//             for(int i = 0; i < nrOfBytes; i++)
//             {
//                 // ble.putc(bytes[i]);
//                 bleDev.SendByte(bytes[i]);
//             }
//             // ble.putc(10);
//             bleDev.SendCmdEnd();
//             // wait_ms(10);
//             // while(ble.readable())
//             //     pc.putc(ble.getc());

//             //pc.printf(" done with custom command\r\n");
//         }
//         break;
//         case 0xD2:
//         {
//             //pc.printf("Sending $$$ command \r\n");
//             bleDev.EnterCommandMode();
            
//             // ble.putc('$');
//             // wait_ms(100);
//             // ble.putc('$');
//             // ble.putc('$');
//         }
//         break;

//         }

//     }
}

// void processBleInput()
// {
//   int recvCnt = 0;
//   uint8_t recvBuf[50];
//   pc.printf("DEBUG: START BLE READ");
//   while(ble.readable())
//   {
//       recvBuf[recvCnt] = ble.getc();
//       recvCnt++;
//       if(recvCnt == 50)
//         break;
//   }
//   for(int echoCnt = 0; echoCnt < recvCnt; echoCnt++)
//       pc.printf("%c", recvBuf[echoCnt]);
// }
