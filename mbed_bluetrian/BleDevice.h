#ifndef BLE_DEVICE_H
#define BLE_DEVICE_H

#include "mbed.h"
#include "Serial.h"
#include "RawSerial.h"

class BleDevice
{
private:
    RawSerial ble;
    RawSerial* pc_p;
    bool scanningActive;
    Timeout reader;


public:
    BleDevice();
    BleDevice(PinName tx_pin, PinName rx_pin, int baud, RawSerial* pc_p);
    ~BleDevice();

    void Process();
    void EnterCommandMode();
    void ProcessBleInput();
    void SendByte(uint8_t byte);
    void SendCmdEnd();
    void SetAdvertisementStrength(uint8_t strength);
    void StartScanning();
    void StopScanning();

};




#endif //BLE_DEVICE_H