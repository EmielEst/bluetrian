#include "mbed.h"
#include "UartMessage.h"

class UartMessageBuilder
{
    public:
        UartMessageBuilder(void);
        ~UartMessageBuilder(void);

        void start_new_message(void);
        void push_byte_to_message(uint8_t p_byte);

        bool message_is_building();
        bool message_is_build();

        UartMessage* get_completed_message(void);
        void reset(void);

    private:
        typedef enum
        {
            build_state_not_building,
            build_state_building, 
            build_state_done
        } build_state_t;

        build_state_t build_state;
        int byte_counter;
        UartMessage* construct_message;
        bool message_complete;

        

}