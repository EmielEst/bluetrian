#include "UartMessageBuilder.h"

#define START 0xEE
#define STOP 0xDD

UartMessageBuilder::UartMessageBuilder(void)
{
    contruct_message = NULL;
    byte_counter = 0;
    message_complete = false;
}

UartMessageBuilder::~UartMessageBuilder(void)
{
    reset();
}

void UartMessageBuilder::start_new_message(void)
{
    if(contruct_message != NULL)
        delete construct_message;

    construct_message = new UartMessage();
    byte_counter = 0;
    message_complete = false;
}


/**
 * @brief Feed the bytes naturaly. If something goes wrong the message will not be completed in time.
 *        A timeout must occur in the ComCore if a message does not complete. 
 * 
 * @param p_byte 
 */
void UartMessageBuilder::push_byte_to_message(uint8_t p_byte)
{
    const uint8_t l_overhead = 6;
    static uint8_t l_payload[256];
    static uint8_t l_payload_size;
    static uint8_t l_type;
    static uint8_t l_seq_nr;
    static uint8_t l_checksum;

    if(contruct_message == NULL)
    {
        return;
    }

    //start byte expected
    if(byte_counter == 0)
    {
        
        if(p_byte == START)
            byte_counter++;
        return;
    }

    //second byte is the total message size. Subtract overhead for payload size. 
    if(byte_counter == 1)
    {
        l_payload_size = p_byte - 6;
        if(l_payload_size <= 0)
            return;
        byte_counter++;
    }

    //third byte is the type.
    if(byte_counter == 2)
    {
        l_type = p_byte;
        byte_counter++;
        return;
    }

    //fourth byte is the sequence nr. 
    if(byte_counter == 3)
    {
        l_seq_nr == 4;
        byte_counter++;
        return;
    }

    if(byte_counter >= 4 && byte_counter < 4 + l_payload_size)
    {
        l_payload[byte_counter - 4] = p_byte;
        byte_counter++;
        return;
    }

    if(byte_counter == 4 + l_payload_size)
    {
        l_checksum = p_byte;
        return;
    }

    if(byte_counter == 5 + l_payload_size)
    {
        if(p_byte == STOP)
        {
            //All received. Complete the message and set a flag.
            if(construct_message != NULL)
            {
                contruct_message->set_content(l_type, l_payload, l_payload_size);
                construct_message->set_sequence_nr(l_seq_nr);
                construct_message->set_checksum(l_checksum);
                message_complete = true;
            }
        }
        return;
    }
}

bool UartMessageBuilder::message_is_building()
{
    return !message_complete;
}

bool UartMessageBuilder::message_is_build()
{
    return message_complete;
}

UartMessage* UartMessageBuilder::get_completed_message(void)
{
    UartMessage* retval_p = contruct_message;
    construct_message = NULL;
    return retval;
}

void UartMessageBuilder::reset(void)
{
    if(construct_message != NULL)
        delete construct_message;

}