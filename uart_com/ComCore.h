#ifndef COMCORE_H
#define COMCORE_H

#include "Buffer.h"
#include "UartMessage.h"
#include "UartMessageBuilder.h"

class ComCore
{
public:
    typedef struct 
    {
        void (*send)(uint8_t*, uint8_t); //send function for serial that takes array of bytes and the nr of bytes
        uint8_t (*recv)(uint8_t*, uint8_t); //recv function. returns nr of bytes and pass an array and it's size to be filled in. 

    } send_recv_api_t;

private:
    typedef enum 
    {
        rx_com_state_idle,
        rx_com_state_rec,
        rx_com_state_complete,
        rx_com_state_unpack,
        rx_com_state_exec,
        rx_com_state_ack,
        rx_com_state_nack,
        rx_com_state_timeout,
        rx_com_state_error
    } rx_com_state_t;

    typedef enum
    {
        tx_com_state_idle,
        tx_com_state_send,
        tx_com_state_wait_for_ack,
        tx_com_state_ack,
        tx_com_state_timeout,
        tx_com_state_nack
    } tx_com_state_t;

    rx_com_state_t cur_rx_com_state;
    tx_com_state_t cur_tx_com_state;

    Buffer<UartMessage*>tx_queue;
    Buffer<UartMessage*>rx_queue;

    send_recv_api_t* send_recv_api;
    int seq_nr_to_ack;

    UartMessageBuilder message_builder;

    

public:
    ComCore(void);
    ~ComCore(void);

    void process(long p_time_ms);
    void tx_data(uint8_t p_id, uint8_t * p_payload_p, uint8_t p_size);

    void set_api(send_recv_api_t* p_api_p);
    
private:
    void process_tx_messages(long p_time_ms);
    void process_rx_messages(long p_time_ms);
    void queue_msg_for_tx();
    void get_rx_msg();

};

#endif //COMCORE_H
