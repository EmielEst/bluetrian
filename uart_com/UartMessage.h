#ifndef UARTMESSAGE_H
#define UARTMESSAGE_H

#include "mbed.h"

class UartMessage
{
private:
    uint8_t *data;
    uint8_t *type;
    uint8_t *length;
    uint8_t *seq_nr;
    uint8_t *payload;
    uint8_t *checksum;


public:
    UartMessage();
    UartMessage(uint8_t p_type, uint8_t *p_payload_p, uint8_t size_p);

    void set_content(uint8_t p_type, uint8_t *p_payload_p, uint8_t size_p);

    void set_sequence_nr(uint8_t p_seq_nr);
    uint8_t get_sequence_nr();

    uint8_t get_length();
    uint8_t* get_data();

    void set_checksum(uint8_t p_checksum);

    bool checksum_correct();


    ~UartMessage();
};

#endif //UARTMESSAGE_H