#include "ComCore.h"

#define CYCLE_SIZE 10
#define CYCLE_TIME_MS 10
#define TIMEOUT 100
#define ACKOK -1
#define ACKNOK -2
#define RX_CYCLE_SIZE 25

ComCore::ComCore(void)
{
    cur_rx_com_state = rx_com_state_idle;
    cur_tx_com_state = tx_com_state_idle;
}

ComCore::~ComCore(void)
{
}

void ComCore::process(long p_time_ms)
{
    process_tx_messages(p_time_ms);
    process_rx_messages(p_time_ms);
}

void ComCore::tx_data(uint8_t p_id, uint8_t * p_payload_p, uint8_t p_size)
{
    UartMessage *l_message = new UartMessage(p_id, p_payload_p, p_size);

    if(tx_queue.add(l_message))
    {
        
    }
    else
    {
        delete l_message;
    }
}

void ComCore::set_api(send_recv_api_t* p_api_p)
{
    send_recv_api = p_api_p;
}

void ComCore::process_tx_messages(long p_time_ms)
{
    static UartMessage* current_message = NULL;
    static uint8_t byte_counter = 0; 
    static uint8_t seq_nr = 0; 
    static long send_stamp = 0;
    static long ack_stamp = 0;

    uint8_t current_cycle_size;
    uint8_t bytes_remaining;

    switch (cur_tx_com_state)
    {
        case tx_com_state_idle:
        {
            if( (current_message = tx_queue.get()) != NULL )
            {
                cur_tx_com_state = tx_com_state_send;
                current_message->set_sequence_nr(seq_nr);
                byte_counter = 0;
                send_stamp = 0;
            }
        }
        break;
        case tx_com_state_send:
        {
            if(send_stamp == 0 || (p_time_ms - send_stamp > CYCLE_TIME_MS)  )
            {
                send_stamp = p_time_ms;
                //send some bytes, the max amount is fixed. 
                bytes_remaining = current_message->get_length() - byte_counter;
                current_cycle_size = (bytes_remaining > CYCLE_SIZE) ? CYCLE_SIZE : bytes_remaining;
                //send bytes
                send_recv_api->send(current_message->get_data() + byte_counter, current_cycle_size);
                byte_counter += current_cycle_size;
                if(byte_counter >= current_message->get_length() )
                {
                    seq_nr_to_ack = seq_nr;
                    cur_tx_com_state = tx_com_state_wait_for_ack;
                    ack_stamp = p_time_ms; //for timeout. 
                }
            }

        }
        break;
        case tx_com_state_wait_for_ack:
        {
            //check time out. 
            if(p_time_ms - ack_stamp > TIMEOUT)
            {
                //retransmit.
                cur_tx_com_state = tx_com_state_send;
                byte_counter = 0;
                send_stamp = 0;
            }
            else
            {
                //-1 means the message has been acknowledged. 
                //this is done in the receive process. 
                if(seq_nr_to_ack == ACKOK)
                {
                    cur_tx_com_state = tx_com_state_idle;
                    seq_nr++;
                }
                if(seq_nr_to_ack == ACKNOK)
                {
                    //retransmit.
                    cur_tx_com_state = tx_com_state_send;
                    byte_counter = 0;
                    send_stamp = 0;
                }
            }

        }
        break;
        case tx_com_state_ack:
        {
            //unused
        }
        break;
        case tx_com_state_timeout:
        {
            //unused
        }
        break;
        case tx_com_state_nack:
        {
            //unused
        }
        break;
    
        default:
            break;
    }
}

void ComCore::process_rx_messages(long p_time_ms)
{
    static uint8_t l_recv_buffer[RX_CYCLE_SIZE]; //buffer to receive in. Statemachine will decide what to do here.
    uint8_t l_nr_bytes_received;
    static long recv_stamp = 0;
    UartMessage* received_message = NULL;
    static uint8_t seq_nr = 0;

    switch (cur_rx_com_state)
    {
        case rx_com_state_idle:
        {
            l_nr_bytes_received = send_recv_api->recv(l_recv_buffer, RX_CYCLE_SIZE);
            if(l_nr_bytes_received > 0)
            {
                message_builder.start_new_message();
                for(int rec_byte_nr = 0; rec_byte_nr < l_nr_bytes_received; rec_byte_nr++)
                {
                    //Received something. Start processing. 
                    //Feed the data into a new message.
                    message_builder.push_byte_to_message(l_recv_buffer[rec_byte_nr]);
                }
                if(message_builder.message_is_build())
                {
                    //complete message received in one go.
                    cur_rx_com_state = rx_com_state_complete;
                    received_message = message_builder.get_completed_message();
                }
                else
                {
                    //message is not build completely.
                    cur_rx_com_state = rx_com_state_rec;
                    recv_stamp = p_time_ms;
                }
            }
            else
            {
                //nothing received. Stay here. 
                //do nothing.
            }

        }
        break;
        case rx_com_state_rec:
        {
            if(p_time_ms - recv_stamp > TIMEOUT)
            {
                //error. There was a timeout!
            }
            else
            {
                l_nr_bytes_received = send_recv_api->recv(l_recv_buffer, RX_CYCLE_SIZE);
                if(l_nr_bytes_received > 0)
                {
                    for(int rec_byte_nr = 0; rec_byte_nr < l_nr_bytes_received; rec_byte_nr++)
                    {
                        //process bytes received. 
                        message_builder.push_byte_to_message(l_recv_buffer[rec_byte_nr]);
                        recv_stamp = p_time_ms;
                    }
                    if(message_builder.message_is_build())
                    {
                        //complete message received.
                        cur_rx_com_state = rx_com_state_complete;
                        received_message = message_builder.get_completed_message();
                    }
                    else
                    {
                        //do nothing. Stay in this state. 
                    }
                }
                else
                {

                }
            }
        }
        break;
        case rx_com_state_complete:
        {
            //completed message received. Let's ack or nack and add if correct give it to an unpacker.
            if(received_message->checksum_correct())
            {
                //message is good
                if(received_message->get_sequence_nr() > seq_nr)
                {
                    seq_nr = received_message->get_sequence_nr();
                    //add to unpacker because it's new.

                }
                
                //send ack
                cur_rx_com_state = rx_com_state_ack;
            }
            else
            {
                //message is bad.
                cur_rx_com_state = rx_com_state_nack;


            }

        }
        break;
        case rx_com_state_unpack:
        {

        }
        break;
        case rx_com_state_exec:
        {

        }
        break;
        case rx_com_state_ack:
        {

        }
        break;
        case rx_com_state_nack:
        {

        }
        break;
        case rx_com_state_timeout:
        {
            //send nack
        }
        break;
        case rx_com_state_error:
        {

        }
        break;
    
        default:
            break;
    }
    
}