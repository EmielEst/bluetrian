#include "UartMessage.h"

#define START 0xEE
#define STOP 0xDD

UartMessage::UartMessage()
{
    data = NULL;
}

UartMessage::~UartMessage()
{
    if(data != NULL)
        delete data;
}

/**
 * @brief Construct a new Uart Message:: Uart Message object
 * 
 * @param p_type type of the message
 * @param p_payload_p the payload 
 * @param size_p the size of the payload
 */
UartMessage::UartMessage(uint8_t p_type, uint8_t *p_payload_p, uint8_t size_p)
{
    set_content(p_type, p_payload_p, size_p);
}

/**
 * @brief Construct a new UartMessage::set_content object
 * 
 * @param p_type type of the message
 * @param p_payload_p the payload 
 * @param size_p the size of the payload
 */
void UartMessage::set_content(uint8_t p_type, uint8_t *p_payload_p, uint8_t size_p)
{
    const uint8_t overhead = 6; // Start - lenght - type - seqNr - check - stop

    if(data != NULL)
        delete data;

    data = new uint8_t[size_p + overhead];
    data[0] = START;

    length = &data[1];
    *length = size_p + overhead;
    

    type = &data[2];
    *type = p_type;
    

    seq_nr = &data[3];

    payload = &data[4];

    memcpy(payload, p_payload_p, size_p);

    checksum = &data[4 + size_p];

    data[5 + size_p] = STOP;
}

void UartMessage::set_sequence_nr(uint8_t p_seq_nr)
{
    if(data != NULL)
        *seq_nr = p_seq_nr;
}

uint8_t UartMessage::get_sequence_nr()
{
    if(data != NULL)
        return *seq_nr;
    else
        return 0;
}

uint8_t UartMessage::get_length()
{   
    if(data != NULL)
        return *length;
    else
        return 0;
}

uint8_t* UartMessage::get_data()
{
    return data;
}

void UartMessage::set_checksum(uint8_t p_checksum)
{
    *checksum = p_checksum;
}

bool UartMessage::checksum_correct()
{
    //TO DO
    return true;
}