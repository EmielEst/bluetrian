﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geometry_lib
{
    public class Circle
    {
        public PointF center;
        public float radius;

        public Circle(PointF center, float radius)
        {
            this.center = center;
            this.radius = radius;
        }

        /**
         * @fn public void GetShortestCircleDistance(PointF refPoint)
         *
         * @brief Gets shortest distance of a point on the circle to the point we're considering. 
         *
         * @param refPoint The reference point.
         */

        public float GetShortestCircleDistance(PointF refPoint)
        {
            PointF closestPoint;
            if(GetClosestCirclePoint(refPoint, out closestPoint) == 1)
            {
                return closestPoint.GetDistanceToPoint(refPoint);
            }
            return float.NaN;

        }

        /**
         * @fn public int GetCirclesIntersections(Circle secondCircle, out PointF intersection1, out PointF intersection2)
         *
         * @brief Gets circles intersections
         *
         * @param       secondCircle  The second circle.
         * @param [out] intersection1 The first intersection if it exists.
         * @param [out] intersection2 The second intersection if it exists.
         *
         * @returns the number of intersections.
         */

        public int GetCirclesIntersections(Circle secondCircle, out PointF intersection1, out PointF intersection2)
        {
            float cx0 = this.center.x;
            float cy0 = this.center.y;
            float radius0 = this.radius;

            float cx1 = secondCircle.center.x;
            float cy1 = secondCircle.center.y;
            float radius1 = secondCircle.radius;

            // Find the distance between the centers.
            float dx = cx0 - cx1;
            float dy = cy0 - cy1;
            double dist = Math.Sqrt(dx * dx + dy * dy);

            // See how many solutions there are.
            if (dist > radius0 + radius1)
            {
                // No solutions, the circles are too far apart.
                intersection1 = new PointF(float.NaN, float.NaN);
                intersection2 = new PointF(float.NaN, float.NaN);
                return 0;
            }
            else if (dist < Math.Abs(radius0 - radius1))
            {
                // No solutions, one circle contains the other.
                intersection1 = new PointF(float.NaN, float.NaN);
                intersection2 = new PointF(float.NaN, float.NaN);
                return 0;
            }
            else if ((dist == 0) && (radius0 == radius1))
            {
                // No solutions, the circles coincide.
                intersection1 = new PointF(float.NaN, float.NaN);
                intersection2 = new PointF(float.NaN, float.NaN);
                return 0;
            }
            else
            {
                // Find a and h.
                double a = (radius0 * radius0 -
                    radius1 * radius1 + dist * dist) / (2 * dist);
                double h = Math.Sqrt(radius0 * radius0 - a * a);

                // Find P2.
                double cx2 = cx0 + a * (cx1 - cx0) / dist;
                double cy2 = cy0 + a * (cy1 - cy0) / dist;

                // Get the points P3.
                intersection1 = new PointF(
                    (float)(cx2 + h * (cy1 - cy0) / dist),
                    (float)(cy2 - h * (cx1 - cx0) / dist));
                intersection2 = new PointF(
                    (float)(cx2 - h * (cy1 - cy0) / dist),
                    (float)(cy2 + h * (cx1 - cx0) / dist));

                // See if we have 1 or 2 solutions.
                if (dist == radius0 + radius1) return 1;
                return 2;
            }
        }

        /**
         * @fn public int GetClosestCirclePoint(PointF refPoint, out PointF closestPoint)
         *
         * @brief Gets closest circle point
         *
         * @param       refPoint     The reference point.
         * @param [out] closestPoint The closest point.
         *
         * @returns The closest circle point.
         */

        public int GetClosestCirclePoint(PointF refPoint, out PointF closestPoint)
        {
            PointF intersection1;
            PointF intersection2;
            float distance1;
            float distance2;

            int intermediateResult = FindLineCircleIntersections(center, refPoint, out intersection1, out intersection2);

            if(intermediateResult == 2)
            {
                //Get the closest point to the reference point. 
                distance1 = refPoint.GetDistanceToPoint(intersection1);
                distance2 = refPoint.GetDistanceToPoint(intersection2);
                if(distance1 <= distance2)
                {
                    closestPoint = intersection1;
                }
                else
                {
                    closestPoint = intersection2;
                }

                return 1;
            }
            if(intermediateResult == 1)
            {
                closestPoint = intersection1;
                return 1;
            }

            closestPoint = new PointF(float.NaN, float.NaN);
            return 0;
        }

        /**
         * @fn private int FindLineCircleIntersections( PointF point1, PointF point2, out PointF intersection1, out PointF intersection2)
         *
         * @brief Searches the intersections of a line and a circle.
         *
         * @param       point1        The first point.
         * @param       point2        The second point.
         * @param [out] intersection1 The first intersection.
         * @param [out] intersection2 The second intersection.
         *
         * @returns The found line circle intersections.
         */

        private int FindLineCircleIntersections( PointF point1, PointF point2, out PointF intersection1, out PointF intersection2)
        {

            float cx, cy, dx, dy, A, B, C, det, t;
            cx = this.center.x;
            cy = this.center.y;

            dx = point2.x - point1.x;
            dy = point2.y - point1.y;

            A = dx * dx + dy * dy;
            B = 2 * (dx * (point1.x - cx) + dy * (point1.y - cy));
            C = (point1.x - cx) * (point1.x - cx) +
                (point1.y - cy) * (point1.y - cy) -
                radius * radius;

            det = B * B - 4 * A * C;
            if ((A <= 0.0000001) || (det < 0))
            {
                // No real solutions.
                intersection1 = new PointF(float.NaN, float.NaN);
                intersection2 = new PointF(float.NaN, float.NaN);
                return 0;
            }
            else if (det == 0)
            {
                // One solution.
                t = -B / (2 * A);
                intersection1 =
                    new PointF(point1.x + t * dx, point1.y + t * dy);
                intersection2 = new PointF(float.NaN, float.NaN);
                return 1;
            }
            else
            {
                // Two solutions.
                t = (float)((-B + Math.Sqrt(det)) / (2 * A));
                intersection1 =
                    new PointF(point1.x + t * dx, point1.y + t * dy);
                t = (float)((-B - Math.Sqrt(det)) / (2 * A));
                intersection2 =
                    new PointF(point1.x + t * dx, point1.y + t * dy);
                return 2;
            }
        }
    }
}
