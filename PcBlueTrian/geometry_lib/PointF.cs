﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geometry_lib
{
    public class PointF
    {
        public float x;
        public float y;

        public PointF(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        /**
         * @fn public float GetDistanceToPoint(PointF secondPoint)
         *
         * @brief Gets distance to a second point
         *
         * @param secondPoint The second point.
         *
         * @returns The distance to point.
         */

        public float GetDistanceToPoint(PointF secondPoint)
        {
            float dx = this.x - secondPoint.x;
            float dy = this.y - secondPoint.y;
            return (float) Math.Sqrt(dx * dx + dy * dy);
        }

        /**
         * @fn public PointF GetMiddleBetweenPoints(PointF secondPoint)
         *
         * @brief Gets the middle between two points
         *
         * @param secondPoint The second point.
         *
         * @returns The middle between points.
         */

        public PointF GetMiddleBetweenPoints(PointF secondPoint)
        {
            PointF middlePoint = new PointF(float.NaN, float.NaN);

            middlePoint.x = (this.x + secondPoint.x) / 2;
            middlePoint.y = (this.y + secondPoint.y) / 2;
            return middlePoint;
        }
    }
}
