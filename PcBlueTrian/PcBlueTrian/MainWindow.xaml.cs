﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO.Ports;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using static PcBlueTrian.Triangulator;

namespace PcBlueTrian
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BlueTrianDevice device;
        int messageCounter = 0;

        //for dragging
        protected bool isDragging;

        //Circles around the stations. 
        Station[] stations;
        Triangulator trian;


        public MainWindow()
        {
            InitializeComponent();

            LblMsgCount.Content = "No messages received";

            device = new BlueTrianDevice();
            device.SetUiWindow(this);
            BtnConnect.Background = Brushes.DarkRed;
            string[] comports = SerialPort.GetPortNames();
            foreach (string str in comports)
                ComboBoxComPorts.Items.Add(str);

            InitStations();
            trian = new Triangulator(device, stations);
        }

        /**
         * @fn private void WindowClosing()
         *
         * @brief Window closing
         */

        private void WindowClosing(object sender, CancelEventArgs e)
        {
            device.StopComThread();
            trian.StopUpdateThread();
            e.Cancel = false;
        }

        /**
         * @fn private void InitStations()
         *
         * @brief Initializes the stations. A station is the image on the canvas with it's radius.
         */

        private void InitStations()
        {
            const int stationSize = 50;
            stations = new Station[7]; //normally 4
            stations[0] = new Station(10, 10, true, canvas_stations, "pack://application:,,,/PcBlueTrian;component/Images/one.PNG", stationSize);
            stations[1] = new Station(100, 100, true, canvas_stations, "pack://application:,,,/PcBlueTrian;component/Images/two.PNG", stationSize);
            stations[2] = new Station(10, 200, true, canvas_stations, "pack://application:,,,/PcBlueTrian;component/Images/three.PNG",stationSize);
            stations[3] = new Station(100, 100, false, canvas_stations, "pack://application:,,,/PcBlueTrian;component/Images/target.PNG", stationSize);
        }

        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (ComboBoxComPorts.SelectedItem == null)
                return;
            else
            {
                if (device.Connect(ComboBoxComPorts.SelectedItem.ToString()))
                {
                    BtnConnect.Background = Brushes.DeepSkyBlue;
                    device.StartComThead();
                }

                else
                    BtnConnect.Background = Brushes.DarkRed;
            }
        }

        public void UpdateConsole(string data)
        {
            TxtBlockOutput.Inlines.Add(new Run(data));
            messageCounter++;
            LblMsgCount.Content = "Message count: " + messageCounter;
            ScrollViewTxtOutput.ScrollToEnd();
        }

        private void BtnSendCustomCmd_Click(object sender, RoutedEventArgs e)
        {
            device.SendCustomCmd(false, TxtBoxCustomCmd.GetLineText(0));
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            trian.SetEnvironmentalFactor(float.Parse(TxtBoxEnvFac.Text, CultureInfo.InvariantCulture.NumberFormat));
            trian.SetPtxForDistanceCalc(float.Parse(TxtBoxPtx.Text, CultureInfo.InvariantCulture.NumberFormat));
            trian.UpdatePositions();
        }

        private void BtnCalibrate_Click(object sender, RoutedEventArgs e)
        {
            trian.SetEnvironmentalFactor(float.Parse(TxtBoxEnvFac.Text, CultureInfo.InvariantCulture.NumberFormat));
            trian.SetPtxForDistanceCalc(float.Parse(TxtBoxPtx.Text, CultureInfo.InvariantCulture.NumberFormat));
            trian.SetCalibrateFromRssiFlag();
        }

        private void BtnTestTrilateration_Click(object sender, RoutedEventArgs e)
        {
            StationDistances distances= new StationDistances(50, 50, 50);

            trian.UpdateTargetPosition(false, distances);
        }

        /**
         * @fn private SignalStrengths GetSignalStrengthsFromTextBoxes()
         *
         * @brief Gets signal strengths from text boxes
         *
         * @returns The signal strengths from text boxes.
         */
        private SignalStrengths GetSignalStrengthsFromTextBoxes()
        {
            SignalStrengths str = new SignalStrengths();

            str.ssri1 = ConvHexOrDecStringSafe(TxtBoxRssi1.GetLineText(0));
            str.ssri2 = ConvHexOrDecStringSafe(TxtBoxRssi2.GetLineText(0));
            str.ssri3 = ConvHexOrDecStringSafe(TxtBoxRssi3.GetLineText(0));

            return str;
        }

        /**
         * @fn private int ConvHexOrDecStringSafe(string str)
         *
         * @brief Convert hexadecimal or decimal string in a safe way
         *
         * @param str The string representing a hex number, starting with 0x or 0X or a decimal number.
         *
         * @returns The string converted to an int.
         */

        private int ConvHexOrDecStringSafe(string str)
        {
            int retVal;
            try
            {
                if (str.Contains("x") || str.Contains("X"))
                    retVal = Convert.ToInt32(str, 16);
                else
                    retVal = int.Parse(str);
            } 
            catch
            {
                retVal = 10; //a default value;
            }
            return retVal;
        }

        /**
         * @fn private float ConvStringToFloatSafe(string str)
         *
         * @brief Convert string to float in a safe way
         *
         * @param str The string.
         *
         * @returns The string converted to float in a safe way.
         */

        private float ConvStringToFloatSafe(string str)
        {
            float retVal;
            try
            {
                retVal = float.Parse(str, CultureInfo.InvariantCulture.NumberFormat);
            }
            catch(Exception e)
            {
                retVal = 1.0f;
            }

            return retVal;
        }

        private void BtnFakeUpdate_Click(object sender, RoutedEventArgs e)
        {
            string str = TxtBoxEnvFac.Text;
            trian.SetEnvironmentalFactor(ConvStringToFloatSafe(TxtBoxEnvFac.Text));
            trian.SetPtxForDistanceCalc(ConvStringToFloatSafe(TxtBoxPtx.Text));

            SignalStrengths fakeStrenghts = GetSignalStrengthsFromTextBoxes();
            trian.FakeUpdateTargetPosition(fakeStrenghts);
        }

        private void BtnFakeCalibration_Click(object sender, RoutedEventArgs e)
        {
            trian.SetEnvironmentalFactor(float.Parse(TxtBoxEnvFac.Text, CultureInfo.InvariantCulture.NumberFormat));
            trian.SetPtxForDistanceCalc(float.Parse(TxtBoxPtx.Text, CultureInfo.InvariantCulture.NumberFormat));

            SignalStrengths fakeStrenghts = GetSignalStrengthsFromTextBoxes();
            trian.FakeSetCalibrateFromRssiFlag(fakeStrenghts);
        }

    }
}
