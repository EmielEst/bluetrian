﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using geometry_lib;

namespace PcBlueTrian
{
    class Station : Circle
    {
        Canvas parentCanvas;
        Image img;
        Ellipse stationCircle;

        const float startRadius = 0;
        
        //for dragging
        protected bool isDragging;
        private Point clickPosition;
        private Point internalOffset;
        bool showRadius;

        public delegate void UpdateRadiusDel(float newRadius);
        public delegate void UpdatePositionDel(int xPos, int yPos);
        public delegate void UpdateOnCanvasDel();

        float width;
        float height;

        int xPos;
        int yPos;

        int imgDim;

        /**
         * @fn public Station(int xPos, int yPos, bool showRadius, Canvas canvas, string imgSource, int imgDim) : base(new PointF(xPos, yPos), startRadius)
         *
         * @brief Constructor
         *
         * @param xPos       The x-coordinate of the center position.
         * @param yPos       The y-coordinate of the center position.
         * @param showRadius True to show, false to hide the radius and not show it on canvas.
         * @param canvas     The Canvas to show the station on.
         * @param imgSource  The image source.
         * @param imgDim     The image dimension. Width and height are the same dimension.
         */

        public Station(int xPos, int yPos, bool showRadius, Canvas canvas, string imgSource, int imgDim) : base(new PointF(xPos, yPos), startRadius)
        {
            this.parentCanvas = canvas;

            this.imgDim = imgDim;
            img = new Image();
            img.Width = imgDim;
            img.Height = imgDim;
            
            BitmapImage logo = new BitmapImage();
            logo.BeginInit();
            logo.UriSource = new Uri(imgSource);
            logo.EndInit();
            img.Source = logo;

            parentCanvas.Children.Add(img);

            //Update the position. 
            UpdateCenterPosition(xPos, yPos);

            //Should the station have a circle drawn around it on canvas?  
            this.showRadius = showRadius;

            //If a radius needs to be shown, let's make one.
            if (showRadius)
            {
                stationCircle = new Ellipse();
                stationCircle.StrokeThickness = 2;
                stationCircle.Stroke = Brushes.Black;

                stationCircle.Width = radius * 2;
                stationCircle.Height = radius * 2;

                //not sure if still needed? 
                this.width = radius * 2;
                this.height = radius * 2;

                //add to canvas. 
                parentCanvas.Children.Add(stationCircle);
                UpdateRadiusPositionOnCanvas();
            }

            //handlers for drag and dropping. 
            img.MouseLeftButtonDown += new MouseButtonEventHandler(Control_MouseLeftButtonDown);
            img.MouseLeftButtonUp += new MouseButtonEventHandler(Control_MouseLeftButtonUp);
            img.MouseMove += new MouseEventHandler(Control_MouseMove);

            //set the image right for the first time.
            InvokeUpdateOnCanvas();
        }

        /**
         * @fn private void Control_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
         *
         * @brief Event handler. For drag and dropping.
         *
         * @param sender Source of the event.
         * @param e      Mouse button event information.
         */

        private void Control_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isDragging = true;
            var draggableControl = sender as UIElement;
            clickPosition = e.GetPosition(parentCanvas);
            draggableControl.CaptureMouse();

            internalOffset.X = Canvas.GetLeft(draggableControl) - clickPosition.X;
            internalOffset.Y = Canvas.GetTop(draggableControl) - clickPosition.Y;
        }

        /**
         * @fn private void Control_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
         *
         * @brief Event handler. For drag and dropping.
         *
         * @param sender Source of the event.
         * @param e      Mouse button event information.
         */

        private void Control_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isDragging = false;
            var draggable = sender as UIElement;

            //double X = e.GetPosition(this).X;
            //Canvas.SetLeft(draggable, X);
            draggable.ReleaseMouseCapture();
        }

        /**
         * @fn private void Control_MouseMove(object sender, MouseEventArgs e)
         *
         * @brief Event handler. For drag and dropping.
         *
         * @param sender Source of the event.
         * @param e      Mouse event information.
         */

        private void Control_MouseMove(object sender, MouseEventArgs e)
        {
            var draggableControl = sender as UIElement;

            if (isDragging && draggableControl != null)
            {
                Point currentPosition = e.GetPosition(parentCanvas);

                var transform = draggableControl.RenderTransform as TranslateTransform;
                if (transform == null)
                {
                    transform = new TranslateTransform();
                    draggableControl.RenderTransform = transform;
                }

                UpdateCenterPosition((int) (currentPosition.X), (int)(currentPosition.Y));
                Canvas.SetLeft(draggableControl, xPos);
                Canvas.SetTop(draggableControl, yPos);

                if (showRadius)
                {
                    UpdateRadiusPositionOnCanvas();
                }
            }
        }

        /**
         * @fn private void UpdateRadiusPositionOnCanvas()
         *
         * @brief Updates the radius on the canvas
         */

        private void UpdateRadiusPositionOnCanvas()
        {
            if(showRadius)
            {
                Canvas.SetLeft(stationCircle, Canvas.GetLeft(img) - stationCircle.Width / 2 + img.Width / 2);
                Canvas.SetTop(stationCircle, Canvas.GetTop(img) - stationCircle.Height / 2 + img.Height / 2);
            }
        }

        /**
         * @fn public void UpdateRadius(float newRadius)
         *
         * @brief Updates the size of the radius.
         *
         * @param newRadius The new radius.
         */

        public void UpdateRadius(float newRadius)
        {
            if(showRadius)
            {
                radius = newRadius;
                this.width = newRadius * 2;
                this.height = newRadius * 2;
            }
        }

        /**
         * @fn public void InvokeUpdateOnCanvas()
         *
         * @brief Executes the update on canvas on a different thread, and waits for the result
         */

        public void InvokeUpdateOnCanvas()
        {
            img.Dispatcher.Invoke(
                new UpdateOnCanvasDel(this.UpdateOnCanvas)
            );
        }

        /**
         * @fn public void UpdateOnCanvas()
         *
         * @brief Updates the stations representation on canvas.
         */

        public void UpdateOnCanvas()
        {
            Canvas.SetLeft(img, xPos);
            Canvas.SetTop(img, yPos);
            if(showRadius)
            {
                stationCircle.Width = (int)radius * 2;
                stationCircle.Height = (int)radius * 2;
                Canvas.SetLeft(stationCircle, Canvas.GetLeft(img) - stationCircle.Width / 2 + img.Width / 2);
                Canvas.SetTop(stationCircle, Canvas.GetTop(img) - stationCircle.Height / 2 + img.Height / 2);
            }

        }

        /**
         * @fn public void UpdateCenterPosition(int x, int y)
         *
         * @brief Updates the position. The coordinates represent the center.
         *
         * @param x The position.
         * @param y The position.
         */

        public void UpdateCenterPosition(int x, int y)
        {
            center.x = x;
            center.y = y;
            UpdateImgPosition(x - imgDim / 2, y - imgDim / 2);
        }

        /**
         * @fn private void UpdateImgPosition(int xPos, int yPos)
         *
         * @brief Updates the position from the top left of the station.
         *
         * @param xPos The position.
         * @param yPos The position.
         */

        private void UpdateImgPosition(int xPos, int yPos)
        {
            this.xPos = xPos;
            this.yPos = yPos;
            //UpdateRadiusPosition();
        }

        /**
         * @fn public void IncrementRadius()
         *
         * @brief Increment radius
         */

        public void IncrementRadius()
        {
            radius += 1;
        }

        /**
         * @fn public float GetDiameter()
         *
         * @brief Gets the diameter
         *
         * @returns The diameter.
         */

        public float GetDiameter()
        {
            return radius * 2;
        }

        /**
         * @fn public PointF GetCenter()
         *
         * @brief Gets the center
         *
         * @returns The center.
         */

        public PointF GetCenter()
        {
            return center;
        }
    }
}
