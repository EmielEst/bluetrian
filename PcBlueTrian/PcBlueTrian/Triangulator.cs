﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using geometry_lib;

namespace PcBlueTrian
{
    class Triangulator
    {
        private BlueTrianDevice device;

        private bool updateFlag;
        private bool updateTargetPositionFlag;
        private bool calibrateFromRsiiFlag;
        private System.Threading.Thread updateThread;

        private Station[] stations;
        private Station target;

        private float[] stationDistances;

        private SignalStrengths strenghtsToUpdateTargetFrom;
        private StationDistances distancesToUpdateTargetFrom;

        private float pixelsPerMetersRatio;
        private float[] pixelsPerMetersRatios;

        private float ptx;
        private float environmentalFactor;

        public enum eStationName
        {
            eStationOne = 0,
            eStationTwo,
            eStationThree
        }

        public struct SignalStrengths
        {
            public int ssri1;
            public int ssri2;
            public int ssri3;

            public SignalStrengths(int ssri1, int ssri2, int ssri3)
            {
                this.ssri1 = ssri1;
                this.ssri2 = ssri2;
                this.ssri3 = ssri3;
            }
        }

        public struct StationDistances
        {
            public float dist1;
            public float dist2;
            public float dist3;

            public StationDistances(float dist1, float dist2, float dist3)
            {
                this.dist1 = dist1;
                this.dist2 = dist2;
                this.dist3 = dist3;
            }
        }

        /**
         * @fn public Triangulator(BlueTrianDevice device, Station[] stations)
         *
         * @brief Constructor for the trilanulation object.
         *
         * @param device   The mbed device object who's position will be trilaterated.
         * @param stations The stations that are shown in the gui.
         */

        public Triangulator(BlueTrianDevice device, Station[] stations)
        {
            this.device = device;
            this.stations = stations;
            pixelsPerMetersRatio = 0;

            //Flags to trigger stuff from the ui thread. 
            updateFlag = false;
            updateTargetPositionFlag = false;

            //Start a thread that will monitor the flags. 
            updateThread = new System.Threading.Thread(new System.Threading.ThreadStart(UpdateThread));
            updateThread.Start();
            
            //The target is the fourth station. 
            target = stations[3];


            stationDistances = new float[3];
            pixelsPerMetersRatios = new float[3];
        }

        /**
         * @fn public void UpdatePositions()
         *
         * @brief Request an update of the positions of the stations on canvas.
         */

        public void UpdatePositions()
        {
            if (device.IsConnected())
            {
                SetUpdateFlag();
            }
        }

        /**
         * @fn public void UpdateTargetPosition(bool immediate, StationDistances distances)
         *
         * @brief Updates the target position from the distances to each station.
         *
         * @param immediate True to run in this thread.
         * @param distances False to trigger it from the ui.
         */

        public void UpdateTargetPosition(bool immediate, StationDistances distances)
        {
            int numberOfIntersects;
            Station[] intersectStations;

            PointF intersection1;
            PointF intersection2;

            float distance1, distance2;

            PointF closestPointToThird;
            PointF closestPointOnThird;

            PointF middlePoint;

            int expansionCounter = 0;
            const int maxExpansionCount = 1000;
            bool intersectStationsFound = false;

            if (!immediate)
            {
                updateTargetPositionFlag = true;
                distancesToUpdateTargetFrom = distances;
            }
            else
            {
                UpdateStations(distances);

                intersectStationsFound = GetIntersecionStations(out intersectStations);
                //Search 2 circles that intersect. Expand until intersection or until max count is reached. Do this to avoid unlimited expansion.
                while (!intersectStationsFound && expansionCounter < maxExpansionCount)
                {
                    ExpandStationsForIntersection();
                    intersectStationsFound = GetIntersecionStations(out intersectStations);
                    
                    //Sleep a little for a nice animation.
                    System.Threading.Thread.Sleep(20);

                    expansionCounter++;
                }

                if(intersectStationsFound)
                {
                    //Update all stations on the canvas
                    for (int i = 0; i < 3; i++)
                        stations[i].InvokeUpdateOnCanvas();

                    //From the 2 stations, get the intersection points. 
                    numberOfIntersects = intersectStations[0].GetCirclesIntersections(intersectStations[1], out intersection1, out intersection2);

                    //From the 2 intersection points, get the closest one to the third circle. 
                    if (numberOfIntersects == 1)
                    {
                        closestPointToThird = intersection1;
                    }
                    else
                    {
                        //Get the closest point to the third circle. 
                        distance1 = intersectStations[2].GetShortestCircleDistance(intersection1);
                        distance2 = intersectStations[2].GetShortestCircleDistance(intersection2);
                        if (distance1 <= distance2)
                        {
                            closestPointToThird = intersection1;
                        }
                        else
                        {
                            closestPointToThird = intersection2;
                        }
                    }

                    target.UpdateCenterPosition((int)closestPointToThird.x, (int)closestPointToThird.y);

                    //get the closest point on the third circle
                    if (intersectStations[2].GetClosestCirclePoint(closestPointToThird, out closestPointOnThird) == 1)
                    {
                        //get the middle between these 2 points.
                        middlePoint = closestPointToThird.GetMiddleBetweenPoints(closestPointOnThird);
                        //set the target to this position. 
                        target.UpdateCenterPosition((int)middlePoint.x, (int)middlePoint.y);
                        target.InvokeUpdateOnCanvas();
                    }
                }

                updateTargetPositionFlag = false;
            }
        }

        public void TestGetClosest()
        {
            PointF closestPoint;
            Console.WriteLine("testing get closest");
            stations[4].UpdateCenterPosition(300, 150);
            stations[4].UpdateRadius(100);
            stations[4].GetClosestCirclePoint(new PointF(100, 150), out closestPoint);
            stations[5].UpdateCenterPosition((int)closestPoint.x, (int)closestPoint.y);
            stations[4].InvokeUpdateOnCanvas();
            stations[5].InvokeUpdateOnCanvas();
        }

        /**
         * @fn public void ExpandStationsForIntersection()
         *
         * @brief Expand the station radius so that we can search for an intersection. The radius is
         * updated on canvas.
         */

        public void ExpandStationsForIntersection()
        {
            for(int i = 0; i < 3; i++)
            {
                stations[i].IncrementRadius();
                stations[i].InvokeUpdateOnCanvas();
            }
        }

        /**
         * @fn public bool GetIntersecionStations(out Station[] intersectionStations)
         *
         * @brief Gets intersecion stations
         *
         * @param [out] intersectionStations
         * The intersection stations. The third one is the one not yet
         * considered.
         *
         * @returns True if it succeeds, false if it fails.
         */

        public bool GetIntersecionStations(out Station[] intersectionStations)
        {
            PointF intersection1, intersection2;
            intersectionStations = new Station[3];
            if(stations[0].GetCirclesIntersections(stations[1], out intersection1, out intersection2) > 0)
            {
                intersectionStations[0] = stations[0];
                intersectionStations[1] = stations[1];
                intersectionStations[2] = stations[2];
                return true;
            }
            if (stations[1].GetCirclesIntersections(stations[2], out intersection1, out intersection2) > 0)
            {
                intersectionStations[0] = stations[1];
                intersectionStations[1] = stations[2];
                intersectionStations[2] = stations[0];
                return true;
            }
            if (stations[2].GetCirclesIntersections(stations[0], out intersection1, out intersection2) > 0)
            {
                intersectionStations[0] = stations[2];
                intersectionStations[1] = stations[0];
                intersectionStations[2] = stations[1];
                return true;
            }

            return false;
        }

        /**
         * @fn private void SetUpdateFlag()
         *
         * @brief Sets update flag and request an update of the target position.
         *        This triggers a trilateration proces.
         */

        private void SetUpdateFlag()
        {
            updateFlag = true;
        }

        /**
         * @fn private void UpdateThread()
         *
         * @brief Update thread that triggers on the flags set by the ui. It checks once every 200ms.
         * Execution can take longer than 200 ms.
         */

        private void UpdateThread()
        {
            SignalStrengths recStrenghts;
            StationDistances distances;

            while(true)
            {
                if(updateFlag || calibrateFromRsiiFlag)
                {

                    //Get the rssi data from the device. 
                    //recStrenghts = device.GetRssiData(true);
                    recStrenghts = device.BuildRssiStrenghts();

                    //Update the radii
                    if (updateFlag)
                    {
                        distances.dist1 = CalculateDistanceFromRssi(recStrenghts.ssri1);
                        distances.dist2 = CalculateDistanceFromRssi(recStrenghts.ssri2);
                        distances.dist3 = CalculateDistanceFromRssi(recStrenghts.ssri3);
                        //UpdateStations(distances);
                        UpdateTargetPosition(true, distances);
                    }
                        
                    if (calibrateFromRsiiFlag)
                        pixelsPerMetersRatio = CalibrateFromRssiAndPos(recStrenghts);

                    updateFlag = false;
                    calibrateFromRsiiFlag = false;
                }
                if(updateTargetPositionFlag)
                {
                    UpdateTargetPosition(true, distancesToUpdateTargetFrom);
                }
                System.Threading.Thread.Sleep(200);
            }
        }

        /**
         * @fn public void StopUpdateThread()
         *
         * @brief Stops update thread
         */

        public void StopUpdateThread()
        {
            updateThread.Abort();
        }

        /**
         * @fn private void UpdateStations(StationDistances distances)
         *
         * @brief Updates the stations
         *
         * @param distances The record strenghts.
         */

        private void UpdateStations(StationDistances distances)
        {
            stations[0].UpdateRadius(distances.dist1 * pixelsPerMetersRatios[0]);
            stations[1].UpdateRadius(distances.dist2 * pixelsPerMetersRatios[1]);
            stations[2].UpdateRadius(distances.dist3 * pixelsPerMetersRatios[2]);

            stations[0].InvokeUpdateOnCanvas();
            stations[1].InvokeUpdateOnCanvas();
            stations[2].InvokeUpdateOnCanvas();
        }

        /**
         * @fn public void SetCalibrateFromRssiFlag()
         *
         * @brief Sets calibrate from rssi flag and requests a calibration. This triggers a trilateration process. 
         */

        public void SetCalibrateFromRssiFlag()
        {
            if(device.IsConnected())
                calibrateFromRsiiFlag = true;
        }

        /**
         * @fn public void FakeSetCalibrateFromRssiFlag(SignalStrengths fakeStrenghts)
         *
         * @brief Fake set calibrate from rssi flag
         *
         * @param fakeStrenghts The fake strenghts.
         */

        public void FakeSetCalibrateFromRssiFlag(SignalStrengths fakeStrenghts)
        {
            pixelsPerMetersRatio = CalibrateFromRssiAndPos(fakeStrenghts);
        }

        /**
         * @fn public void FakeUpdateTargetPosition(SignalStrengths fakeStrenghts)
         *
         * @brief Fake update target position
         *
         * @param fakeStrenghts The fake strenghts.
         */

        public void FakeUpdateTargetPosition(SignalStrengths fakeStrenghts)
        {
            StationDistances distances = new StationDistances();
            distances.dist1 = CalculateDistanceFromRssi(fakeStrenghts.ssri1);
            distances.dist2 = CalculateDistanceFromRssi(fakeStrenghts.ssri2);
            distances.dist3 = CalculateDistanceFromRssi(fakeStrenghts.ssri3);
            //UpdateStations(distances);
            UpdateTargetPosition(false, distances);
        }

        /**
         * @fn public float CalibrateFromRssiAndPos(SignalStrengths signalStrenghtData)
         *
         * @brief Calibrate from RSSI and position. Calibrates this system. To do this the current position
         * of the stations is used together with the current RSSI. Communication with an mbed is needed
         * for successful calibration.
         *
         * @param signalStrenghtData Information describing the signal strength.
         *
         * @returns the number of pixels per meters.
         */

        public float CalibrateFromRssiAndPos(SignalStrengths signalStrenghtData)
        {
            float[] pixelDistance = new float[3];
            float[] rssiData = { signalStrenghtData.ssri1, signalStrenghtData.ssri2, signalStrenghtData.ssri3 };
            float pixelsPerMeterRatio = 0.0f;
            //Get the distance between station 0 and the center in pixels. 

            for (int i = 0; i < 3; i++)
            {
                pixelDistance[i] = stations[i].GetCenter().GetDistanceToPoint(target.GetCenter());
                
                pixelsPerMetersRatios[i] = pixelDistance[i] / CalculateDistanceFromRssi(rssiData[i]);

                pixelsPerMeterRatio += pixelsPerMetersRatios[i];
            }

            //Divide by three for avarage. 
            pixelsPerMeterRatio = pixelsPerMeterRatio / 3;

            return pixelsPerMeterRatio;
        }

        /**
         * @fn public void SetPtxForDistanceCalc(float ptx)
         *
         * @brief Sets ptx for distance calculation
         *
         * @param ptx The ptx.
         */

        public void SetPtxForDistanceCalc(float ptx)
        {
            this.ptx = ptx;
        }

        /**
         * @fn public void SetEnvironmentalFactor(float envFac)
         *
         * @brief Sets the environmental factor for distance calculation
         *
         * @param envFac The environment fac.
         */

        public void SetEnvironmentalFactor(float envFac)
        {
            environmentalFactor = envFac;
        }

        /**
         * @fn public float CalculateDistanceFromRssi(float rssi)
         *
         * @brief Calculates the distance from rssi
         *
         * @param rssi The rssi.
         *
         * @returns The calculated distance from rssi.
         */

        public float CalculateDistanceFromRssi(float rssi)
        {
            float retVal = 0.0f;
            float pow = 0.0f;
            const float n = 1.5f; //constant factor
            const float ptx = -75.2f; //rssi at 1 m.

            if (rssi > 0)
                rssi = rssi * -1f; //if it's positive for whatever reason, it should be negative. We're not expecting positive values here. 

            pow = (ptx - rssi) / (10 * n);
            retVal = (float)Math.Pow(10, pow);
            return retVal;

        }

        /**
         * @fn public float CalculateDistanceFromRssiLinear(float rssi)
         *
         * @brief Calculates the distance from rssi in a linear partitioned way. 
         *        This is not as good. Use CalculateDistanceFromRssi instead. 
         *
         * @param rssi The rssi.
         *
         * @returns The calculated distance from rssi linear.
         */

        public float CalculateDistanceFromRssiLinear(float rssi)
        {
            //these values are guestimates for good regions for linear interpolation. 
            //The choice of these values is based upon measuring the rssi and comparing it to the distance. 
            const float rssiZone1 = 45; //<= this is the best rssi measured. 
            //const float rssiZone2 = 80;
            const float rssiZone2 = 65;
            const float rssiZone3 = 75;
            //const float rssiZone3 = 82;
            const float rssiZone4 = 85; // <= this is the upper border. 

            const float zone1LinearDist = 0.3f;
            const float zone2LinearDist = 1.0f;
            const float zone3LinearDist = 2.0f;

            if (rssi <= rssiZone1)
                return 0.0f;
            if(rssi > rssiZone1 && rssi <= rssiZone2)
            {
                return ((rssi - rssiZone1) / (rssiZone2 - rssiZone1)) * zone1LinearDist;
            }
            if(rssi > rssiZone2 && rssi <= rssiZone3)
            {
                return (((rssi - rssiZone2) / (rssiZone3 - rssiZone2)) * zone2LinearDist) + zone1LinearDist;
            }
            else
            {
                return (((rssi - rssiZone3) / (rssiZone4 - rssiZone3)) * zone3LinearDist) + zone2LinearDist;
            }
            
        }
    }
}
