﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using static PcBlueTrian.Triangulator;
using System.Windows.Controls;

namespace PcBlueTrian
{
    /**
     * @class BlueTrianDevice
     *
     * @brief A blue trian device.
     */

    class BlueTrianDevice
    {
        private enum EGuiRequest { E_GUI_NO_REQUEST, E_GUI_SEND_CUSTOM_COMMAND };

        private Thread CommThread;
        private volatile EGuiRequest GuiRequest;
        private volatile string CustomCmdStr;

        private TextBlock UiTextBlock;
        private MainWindow win;

        private SerialPort MbedPort;

        /**
         * @fn public BlueTrianDevice()
         *
         * @brief Default constructor
         */

        public BlueTrianDevice()
        {

        }

        /**
         * @fn public bool Connect(string comPort)
         *
         * @brief Opens a serial port to an mbed device.  
         *
         * @param comPort The com port to connect to.
         *
         * @returns True if it succeeds, false if it fails.
         */

        public bool Connect(string comPort)
        {
            if (MbedPort != null)
            {
                if (MbedPort.IsOpen)
                {
                    MbedPort.Close();
                    return false;
                }
            }
            try
            {
                MbedPort = new SerialPort();

                MbedPort.PortName = comPort;
                MbedPort.BaudRate = 115200;
                MbedPort.Parity = Parity.None;
                MbedPort.DataBits = 8;
                MbedPort.StopBits = StopBits.One;

                MbedPort.ReadTimeout = 250;
                MbedPort.WriteTimeout = 250;

                MbedPort.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /**
         * @fn public bool IsConnected()
         *
         * @brief Query if this object is connected on the serial port.
         *
         * @returns True if connected, false if not.
         */

        public bool IsConnected()
        {
            if (MbedPort != null && MbedPort.IsOpen)
                return true;
            return false;
        }

        /**
         * @fn public void StartComThead()
         *
         * @brief Starts the CommThread if there is a connection.
         */

        public void StartComThead()
        {
            if (IsConnected())
            {
                CommThread = new Thread(new ThreadStart(CommThreadProc));
                CommThread.Start();
            }

        }

        /**
         * @fn public void StopComThread()
         *
         * @brief Stops com thread
         */

        public void StopComThread()
        {
            if(CommThread != null)
                CommThread.Abort();
        }

        /**
         * @fn public void CommThreadProc()
         *
         * @brief Communications thread that monitors flags set by the user via the UI. These flags are
         * handles one at a time in a different thread so that the UI does not block.
         */

        public void CommThreadProc()
        {
            string reply = "";

            while (true)
            {
                switch (GuiRequest)
                {
                    case EGuiRequest.E_GUI_NO_REQUEST:
                        break;

                    case EGuiRequest.E_GUI_SEND_CUSTOM_COMMAND:
                        SendCustomCmd(true, CustomCmdStr);
                        GetSerialData(ref reply);
                        break;

                    default:
                        break;
                }

                if (MbedPort.BytesToRead > 0)
                {
                    GetSerialData(ref reply);
                    reply = "";
                }

                //reset request so that another one can happen. 
                GuiRequest = EGuiRequest.E_GUI_NO_REQUEST;

                //Add a sleep as to not hog all resources. 
                System.Threading.Thread.Sleep(100);
            }
        }

        /**
         * @fn private void SetRequestFlag(EGuiRequest requestFlag)
         *
         * @brief Sets request flag if no request is pending.
         *
         * @param requestFlag The request flag. This flag determines the operation that will execute.
         */

        private void SetRequestFlag(EGuiRequest requestFlag)
        {
            if(GuiRequest == EGuiRequest.E_GUI_NO_REQUEST)
            {
                GuiRequest = requestFlag;
            }
        }

        /**
         * @fn public bool GetSerialData(ref string data)
         *
         * @brief Gets serial data from the mbed.
         *
         * @param [in,out] data The data in string format.
         *
         * @returns True if it succeeds, false if it fails.
         */

        public bool GetSerialData(ref string data)
        {
            int maxSize = 100;
            byte[] recBuff = new byte[maxSize];
            bool receiving = true;
            bool retVal = false;
            data = "";
            try
            {

                while(receiving)
                {
                    int nrReceived = MbedPort.Read(recBuff, 0, maxSize);
                    if (nrReceived == 0)
                        receiving = false;
                    else
                    {
                        retVal = true;
                        byte[] validData = new byte[nrReceived];
                        Array.Copy(recBuff, 0, validData, 0, nrReceived);
                        data += System.Text.Encoding.UTF8.GetString(validData);
                        Console.WriteLine(data);
                        UpdateUiTextBlock(data);
                    }
                }
                return retVal;

            }
            catch (Exception e)
            {
                Console.WriteLine("Emiel" + e.Message);
                return retVal;
            }
        }

        /**
         * @fn public void UpdateUiTextBlock(string str)
         *
         * @brief Updates the user interface text block described by str
         *
         * @param str The string.
         */

        public void UpdateUiTextBlock(string str)
        {
            win.Dispatcher.Invoke(delegate {
                win.UpdateConsole(str);
            });

        }

        /**
         * @fn private void SendByte(Byte byteToSend)
         *
         * @brief Sends a byte to the device.
         *
         * @param byteToSend The byte to send.
         */

        private void SendByte(Byte byteToSend)
        {
            try
            {
                byte[] cmd = { byteToSend };
                MbedPort.Write(cmd, 0, 1);
            }
            catch
            {
                Console.WriteLine("couldn't write bytes");
            }
        }

        /**
         * @fn public SignalStrengths BuildRssiStrenghts()
         *
         * @brief Gets rssi data of all stations from the bluetooth base station.
         *
         * @returns The rssi data in a SignalStrenghts struct.
         *
         * ### param immediate True to immediate. False to set a flag to do it in anohter thread.
         */

        public SignalStrengths BuildRssiStrenghts()
        {
            SignalStrengths strenghts = new SignalStrengths();
            Resetbrute();
            ConnectToStationBrute(1);
            strenghts.ssri1 = RequestRssiBrute();
            Console.WriteLine("The rssi is " + strenghts.ssri1.ToString());
            Resetbrute();
            ConnectToStationBrute(2);
            strenghts.ssri2 = RequestRssiBrute();
            Console.WriteLine("The rssi is " + strenghts.ssri2.ToString());
            Resetbrute();
            ConnectToStationBrute(3);
            strenghts.ssri3 = RequestRssiBrute();
            Console.WriteLine("The rssi is " + strenghts.ssri3.ToString());
            return strenghts;
        }

        /**
         * @fn private void Resetbrute()
         *
         * @brief Reset the BLE-module
         */

        private void Resetbrute()
        {
            string reply = "";
            GetSerialData(ref reply);
            SendCustomCmd(true, "$$$");
            GetSerialData(ref reply);
            SendCustomCmd(true, "R,1");
            GetSerialData(ref reply);
            //Reboot takes at least 200 ms. Wait long enough.  
            System.Threading.Thread.Sleep(300);
            SendCustomCmd(true, "$$$");
            GetSerialData(ref reply);
        }

        /**
         * @fn private void ConnectToStationBrute(int stationNr)
         *
         * @brief Connects to station that was previously bonded
         *
         * @param stationNr The station nr to connect to.
         */

        private void ConnectToStationBrute(int stationNr)
        {
            string reply = "";
            SendCustomCmd(true, "c" + stationNr.ToString());
            for(int i = 0; i< 10; i++)
            {
                System.Threading.Thread.Sleep(200);
                GetSerialData(ref reply);
                Console.WriteLine(reply);
            }
        }

        /**
         * @fn private int RequestRssiBrute()
         *
         * @brief Request the RSSI from a connected station.
         *
         * @returns The RSSI
         */

        private int RequestRssiBrute()
        {
            string subString;
            int subStringLenght;
            string reply = "";
            int retVal = 0;
            GetSerialData(ref reply);
            SendCustomCmd(true, "$$$");
            GetSerialData(ref reply);
            SendCustomCmd(true, "M");
            GetSerialData(ref reply);
            try
            {
                subStringLenght = reply.Length - 8;
                if(subStringLenght > 0)
                {
                    subString = reply.Substring(1, subStringLenght);
                    retVal = int.Parse(subString, System.Globalization.NumberStyles.HexNumber);
                }

            }
            catch
            {
                
            }

            return retVal;
        }

        public void SetUiTextBlock(TextBlock textBlock)
        {
            UiTextBlock = textBlock;
        }

        public void SetUiWindow(MainWindow win)
        {
            this.win = win;
        }

        /**
         * @fn public void SendCustomCmd(bool immediate, string cmd)
         *
         * @brief Sends a custom command.
         *
         * @param immediate True to send immediate. False to set a flag and set later in other thread.
         * @param cmd       The command to send.
         */

        public void SendCustomCmd(bool immediate, string cmd)
        {
            if(immediate)
            {
                for (int i = 0; i < cmd.Length; i++)
                {
                    System.Threading.Thread.Sleep(20);
                    SendByte(Convert.ToByte(cmd[i]));
                }
                //send delimiter to mark end of command. 
                System.Threading.Thread.Sleep(20);
                SendByte(13);
            }
            else
            {
                CustomCmdStr = cmd;
                SetRequestFlag(EGuiRequest.E_GUI_SEND_CUSTOM_COMMAND);
            }
        }

        //Dead content below.
        

        public void SendResetCommand()
        {
            SendCustomCmd(false, "R,1");
        }

        public void SendDollarCommand()
        {
            SendCustomCmd(false, "$$$");
        }

        public void SendDashedCommand()
        {
            SendCustomCmd(false, "---");
        }

        public void SetHiddenAddress(string hiddenAddress)
        {
            SendCustomCmd(false, "&," + hiddenAddress);
        }

        public void ConnectToHiddenAddress(string hiddenAddress)
        {
            SendCustomCmd(false, "C,1," + hiddenAddress);
        }

        public void SetRtsHi()
        {
            //SendPreCmd();
            //SendByte(RtsHiCmd);
        }

        public void SetRtsLo()
        {
            //SendPreCmd();
            //SendByte(RtsLoCmd);
        }

        public void SetRstHi()
        {
            //SendPreCmd();
            //SendByte(RstHiCmd);
        }

        public void SetRstLo()
        {
            //SendPreCmd();
            //SendByte(RstLoCmd);
        }

        private void SendPreCmd()
        {
            //SendByte(PreCmd);
        }

        public void SetAdStrength()
        {
            //SendPreCmd();
            //SendByte(SetAdStrengthCmd);
        }

        public void UpdateRssiData()
        {
            //SendPreCmd();
            //SendByte(UpdateRssiCmd);
        }

        //end of dead content. 

    }
}
